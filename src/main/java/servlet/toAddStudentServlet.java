package servlet;

import com.sun.net.httpserver.HttpServer;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import util.TemplateEngineUtil;

import javax.jws.WebService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/toAddStudentServlet")
public class toAddStudentServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.service(req, resp);
        //创建引擎
        TemplateEngine engine= TemplateEngineUtil.getTemplateEngine(req.getServletContext());
        //构造应用的上下文环境
        WebContext context = new WebContext(req, resp, req.getServletContext());
        //相应页面设置字符集utf-8
        resp.setCharacterEncoding("utf-8");
        //引擎转向到addStudent.html页面
        //只有引擎处理过的页面才会被解析th标签
        engine.process("addStudent.html",context,resp.getWriter());
    }
}

