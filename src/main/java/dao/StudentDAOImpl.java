package dao;

import domain.Student;
import util.DBUtil;

import java.sql.ResultSet;

public class StudentDAOImpl {
    //获取student表数据
    public ResultSet getStudents(){
        String sql="select * from student";
        ResultSet rs= DBUtil.executeQuery(sql);
        return rs;
    }

    //insert student
    public boolean addStudent(Student student){
        String sql="insert into student(name,password,sex,clazz,birthday) values(?,?,?,?,?)";
        Object[] params={student.getName(),student.getPassword(),student.getSex(),student.getClazz(),student.getBirthday()};

        return DBUtil.executeUpdate(sql,params);
    }
    //update student
    //delete student
    public boolean deleteStudent(int id){
        String sql="delete from student where id=?";
        Object[] params={id};
        return  DBUtil.executeUpdate(sql,params);
    }

}
