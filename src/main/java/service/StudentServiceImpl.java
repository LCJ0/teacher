package service;

import dao.StudentDAOImpl;
import domain.Student;
import util.DBUtil;

import javax.xml.transform.Result;
import java.sql.ResultSet;
//业务层
public class StudentServiceImpl {
    //创建DAO数据访问层对象
    StudentDAOImpl studentDAO=new StudentDAOImpl();
    //list students
    public ResultSet getStudents(){
//调用数据访问层对象的方法
        return studentDAO.getStudents();
    }

    //add student
    public boolean addStudent(Student student){
        return studentDAO.addStudent(student);
    }
    //delete student
    public boolean deleteStudent(int id){
        return studentDAO.deleteStudent(id);
    }
    //update student
}
